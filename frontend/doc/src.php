<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

define('CLEANHOME', '/opt/clean');
error_reporting(0);

$f=fopen (CLEANHOME.'/doc/CleanLanguageReport.html','r');
if (!$f)
	die ('Failed to open language report.');

$found_body=false;
while (($line=fgets ($f))!==false){
	if ($found_body){
		if (substr ($line,0,7)=='</body>'){
			echo '</div>';
			break;
		}
		echo $line;
	} else if (substr ($line,0,5)=='<body'){
		echo '<div'.substr ($line,5);
		$found_body=true;
	}
}

fclose ($f);
