<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

$banners = json_decode(file_get_contents(__DIR__ . '/banners.json'), true);
foreach ($banners as $banner) {
	$from = strtotime($banner['from']);
	$until = strtotime($banner['until']);
	if ($from > time() || $until < time())
		continue;
	if (isset($banner['useragent']) && !preg_match($banner['useragent'], $_SERVER['HTTP_USER_AGENT']))
		continue;
	echo "<div " .
		"class='banner' " .
		"data-id='{$banner['id']}' " .
		"data-from='{$banner['from']}' " .
		"data-until='{$banner['until']}'>" .
		"<span id='{$banner['id']}'>{$banner['text']}</span>" .
		"</div>";
}
