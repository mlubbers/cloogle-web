<?php
/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

define('CLEAN_HOME', '/opt/clean');

if (empty($_REQUEST['mod'])) {
	http_response_code(400);
	echo '<p>Select a module on the left.</p>';
	exit();
}

$mod = preg_replace('/[^\\w\\/\\. -`]/', '', $_REQUEST['mod']);
$mod = explode('/', $mod, 2);
$lib = $mod[0];
$mod = $mod[1];
$iclordcl = isset($_REQUEST['icl']) ? 'icl' : 'dcl';
$fname = CLEAN_HOME . '/lib/' . $lib . '/lib/' . $mod . '.' . $iclordcl;

if (!is_readable($fname)) {
	http_response_code(404);
	if ($iclordcl == 'dcl' && is_readable(substr_replace($fname, 'icl', strlen($fname)-3))) {
		echo '<p>There is no definition module. Tick "Show implementation" to see the implementation module.</p>';
	} else {
		echo '<p><code>' . $fname . '</code> does not exist.</p>';
		echo '<p>If you believe this is an error, please report it <a href="https://gitlab.com/cloogle/cloogle-web/issues/new">on GitLab</a>.';
	}
	exit();
}

echo file_get_contents($fname);
