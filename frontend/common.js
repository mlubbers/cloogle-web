/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

function pluralise(n, what) {
	return n + ' ' + what + (n == 1 ? '' : 's');
}

function toggleElement(e, className) {
	e.classList.toggle(typeof className == 'undefined' ? 'visible' : className);
}

function toggle(toggler, open) {
	var e = toggler;
	while (!e.classList.contains('toggle-container'))
		e = e.parentNode;
	var es = e.getElementsByClassName('togglee');

	for (var i = 0; i < es.length; i++) {
		var p = es[i];
		while (!p.classList.contains('toggle-container'))
			p = p.parentNode;
		if (p != e)
			continue;

		if (typeof open == 'undefined')
			toggleElement(es[i], 'toggle-visible');
		else if (open)
			es[i].classList.add('toggle-visible');
		else
			es[i].classList.remove('toggle-visible');
	}

	var icons = e.getElementsByClassName('toggle-icon');
	for (var i = 0; i < icons.length; i++) {
		var p = es[i];
		while (!p.classList.contains('toggle-container'))
			p = p.parentNode;
		if (p != e)
			continue;

		if (typeof open == 'undefined')
			icons[i].innerHTML = icons[i].innerHTML == '\u229e' ? '&#x229f;' : '&#x229e;';
		else
			icons[i].innerHTML = open ? '&#x229f;' : '&#x229e;';
	}
}

function toggleById(name) {
	toggleElement(document.getElementById(name));
}

function highlightCallback(span, cls, str) {
	var url = window.location.protocol + '//' + window.location.host + '#';
	if (cls == 'type') {
		return '<a class="hidden" title="Search type ' + str + '" href="' +
			url+ encodeURIComponent('type ' + str) + '">' +
			span + '</a>';
	} else if (cls == 'classname' || cls == 'classname classname-generic') {
		return '<a class="hidden" title="Search class ' + str + '" href="' +
			url+ encodeURIComponent('class ' + str) + '">' +
			span + '</a>';
	} else if (cls == 'generic') {
		return '<a class="hidden" title="Search generic ' + str + '" href="' +
			url+ encodeURIComponent(str) + '">' +
			span + '</a>';
	} else if (cls == 'funcname' ||
			cls == 'funcname funcname-onlyused' ||
			cls == 'modulename' ||
			cls == 'constructor' ||
			cls == 'abc-instruction') {
		return '<a class="hidden" title="Search for ' + str + '" href="' +
			url+ encodeURIComponent(str) + '">' +
			span + '</a>';
	} else {
		return span;
	}
}

function highlightQuery(query) {
	var highlighter = highlightClean;
	if (query == 'class' || query == 'instance') {
		return '<span class="keyword">' + query + '</span>';
	} else if (query.match(/^class\s/) || query.match(/^instance\s/)) {
		highlighter = highlightClean;
	} else if (query.match(/^type\s/)) {
		highlighter = function(q) {
			return '<span class="keyword">type</span><span class="whitespace">' + q.substring(4,5) + '</span>' +
				highlightType(q.substring(5));
		};
	} else if (query.match(/^using\s/) || query.match(/^exact\s/)) {
		highlighter = function(q) {
			return '<span class="keyword">' + q.substring(0,5) + '</span><span class="whitespace">' + q.substring(5,6) + '</span>' +
				highlightToHTML({
					start: [
						[/(;)/,     ['punctuation']],
						[/([^;]+)/, ['funcname']],
					]
				}, q.substring(6));
		};
	} else if (query.match(/^\s*::/)) {
		highlighter = function(q) {
			return '<span class="whitespace">' +
				highlightType(q.substring(0, q.indexOf('::'))) +
				'</span><span class="punctuation">::</span>' +
				highlightType(q.substring(q.indexOf('::') + 2));
		};
	} else if (query.match(/\s/) && query.indexOf('::') < 0) {
		return '<span>' + query + '</span>';
	}
	return highlighter(query);
}

var banners = document.getElementsByClassName('banner');
for (let i = 0; i < banners.length; i++) {
	let banner = banners[i];
	banner.dataset.index = i;

	let id = banner.dataset.id;
	if (window.localStorage['banner-' + id] == 'hidden')
		continue;

	var from = new Date(banner.dataset.from);
	var until = new Date(banner.dataset.until);
	var now = new Date();
	if (from <= now && now <= until)
		banner.style.display = 'block';

	var hidelink = document.createElement('a');
	hidelink.classList.add('hidelink');
	hidelink.innerHTML = '&times;';
	hidelink.setAttribute('href', '#');
	hidelink.setAttribute('title', 'Hide this banner forever');
	hidelink.onclick = function () {
		banner.remove();
		window.localStorage['banner-' + id] = 'hidden';
	}
	banner.appendChild(hidelink);
}
