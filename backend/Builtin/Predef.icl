implementation module Builtin.Predef

/**
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

import StdArray
import StdBool
import StdEnum
import StdInt
import StdList
import StdMisc
import StdOverloaded
import StdString

from Data.Func import $
import Data.List
import Data.Maybe
import Text

import Clean.Doc
import Clean.Types

import Cloogle.API
import Cloogle.DB

CLR :: !String -> CleanLangReportLocation
CLR sec =
	{ clr_version = "3.0"
	, clr_section = sec
	}

builtin_functions :: [FunctionEntry]
builtin_functions =
	[ { zero
	  & fe_loc  = Builtin "if" [CLR "3.4.2"]
	  , fe_type = ?Just $ Func [Type "Bool" [], Var "a", Var "a"] (Var "a") (TypeContext [])
	  }
	, { zero
	  & fe_loc  = Builtin "dynamic" [CLR "8.1"]
	  , fe_type = ?Just $ Func [Var "a"] (Type "Dynamic" []) (TypeContext [Instance "TC" [Var "a"]])
	  }
	]

builtin_classes :: [ClassEntry]
builtin_classes =
	[ { ce_loc           = Builtin "TC" [CLR "8.1.4"]
	  , ce_vars          = [Var "v"]
	  , ce_is_meta       = False
	  , ce_context       = TypeContext []
	  , ce_documentation = ?None
	  , ce_members       = {}
	  , ce_instances     = {}
	  , ce_derivations   = {}
	  , ce_usages        = {}
	  , ce_module_usages = 0
	  }
	]

builtin_types :: [TypeDefEntry]
builtin_types =
	[ { deft
	  & tde_loc=Builtin "Bool" [CLR "4.1"]
	  , tde_typedef.td_name = "Bool"
	  , tde_typedef.td_rhs  = TDRCons False
	    [ { defc & cons_name="False" }
	    , { defc & cons_name="True" }
	    ]
	  }
	, { deft & tde_loc=Builtin "Int"     [CLR "4.1"], tde_typedef.td_name = "Int"}
	, { deft & tde_loc=Builtin "Real"    [CLR "4.1"], tde_typedef.td_name = "Real"}
	, { deft & tde_loc=Builtin "Char"    [CLR "4.1"], tde_typedef.td_name = "Char"}
	, { deft & tde_loc=Builtin "Dynamic" [CLR "8"],   tde_typedef.td_name = "Dynamic"}
	, { deft & tde_loc=Builtin "File"    [CLR "4.7"], tde_typedef.td_name = "File"}
	, { deft
	  & tde_loc=Builtin "String" [CLR "4.7"]
	  , tde_typedef.td_name = "String"
	  , tde_typedef.td_rhs = TDRSynonym (Type "_#Array" [Type "Char" []]) }
	, { deft
	  & tde_loc=Builtin "World" [CLR "4.7"], tde_typedef.td_name = "World"
	  , tde_typedef.td_uniq = True
	  , tde_doc = ?Just
	    { TypeDoc | gDefault{|*|}
	    & description = ?Just "An object of this type is automatically created when the program is started, if needed. It makes efficient interfacing with the outside world possible. Its value is always `65536`."
	    }
	  }
	, { deft
	  & tde_loc=Builtin "->" [CLR "4.6"]
	  , tde_typedef.td_name = "(->)"
	  , tde_typedef.td_args = [Var "a", Var "b"]
	  , tde_doc = ?Just
	    { TypeDoc | gDefault{|*|}
	    & description = ?Just "The arrow type is used to denote functions.\n\nOften, function types can be written in an uncurried fashion, e.g. `a b -> c` is the same as `a -> b -> c`."
	    , vars = ["The argument type", "The result type"]
	    }
	  }
	, { deft
	  & tde_loc=Builtin "()" []
	  , tde_typedef.td_name="_Unit"
	  , tde_doc = ?Just
	    { TypeDoc | gDefault{|*|}
	    & description = ?Just "The void / unit type."
	    }
	  , tde_typedef.td_rhs = TDRCons False [{defc & cons_name="()"}]
	  }
	:  lists
	++ arrays
	++ maybes
	++ tuples
	]
where
	deft =
		{ tde_loc           = zero
		, tde_typedef       =
			{ td_name = ""
			, td_uniq = False
			, td_args = []
			, td_rhs  = TDRAbstract ?None
			}
		, tde_doc           = ?None
		, tde_instances     = {}
		, tde_derivations   = {}
		, tde_usages        = {}
		, tde_module_usages = 0
		}
	defc =
		{ cons_name     = ""
		, cons_args     = []
		, cons_exi_vars = []
		, cons_context  = TypeContext []
		, cons_priority = ?None
		}

	lists = [make_list kind spine \\ kind <- [[], ['#'], ['!']], spine <- [[], ['!']]]
	where
		make_list :: [Char] [Char] -> TypeDefEntry
		make_list k s =
			{ deft
			& tde_loc = Builtin higherorder [CLR "4.2"]
			, tde_typedef.td_name = toString (['_':k] ++ ['List'] ++ s)
			, tde_typedef.td_args = [Var "a"]
			, tde_doc = ?Just
				{ TypeDoc | gDefault{|*|}
				& description = ?Just $ "A" + kind + spine + " list.\n\n" + description
				, vars = ["The type of the list elements."]
				}
			}
		where
			higherorder = toString (['[':k] ++ s` ++ [']'])
				with s` = if (s == ['!'] && k == []) [' !'] s
			lista       = toString (['[':k] ++ ['a':s] ++ [']'])
			listints    = toString (['[':k] ++ ['1,1,2,3,5':s] ++ [']'])
			listany     = toString (['[':k] ++ ['\\','w':s] ++ [']'])
			kind = case k of
				[]    ->  " normal"
				['#'] -> "n unboxed"
				['!'] ->  " head strict"
				_     -> abort "error in make_list\n"
			spine = case s of
				[]    -> ""
				['!'] -> " spine strict"
				_     -> abort "error in make_list\n"

			description = "These types of list are available:\n\n" +
				"- {{`[a]`}}, a normal list\n" +
				"- {{`[#a]`}}, an unboxed head-strict list (elements are stored directly, without pointers)\n" +
				"- {{`[!a]`}}, a head-strict list (the first element is in root normal form)\n" +
				"- {{`[a!]`}}, a spine-strict list (the last element is known)\n" +
				"- {{`[#a!]`}}, an unboxed spine-strict list\n" +
				"- {{`[!a!]`}}, a head-strict spine-strict list\n\n" +
				"In expressions, `[|...]` can be used for overloaded lists (of any of the types above)."

	arrays = [make_array kind \\ kind <- [[], ['!'], ['#'], ['32#']]]
	where
		make_array :: [Char] -> TypeDefEntry
		make_array k =
			{ deft
			& tde_loc = Builtin typec [CLR "4.4"]
			, tde_typedef.td_name = toString (['_':k] ++ ['Array'])
			, tde_typedef.td_args = [Var "a"]
			, tde_doc = ?Just
				{ TypeDoc | gDefault{|*|}
				& description = ?Just $ "An array contains a finite number of elements of the same type. Access time is constant.\n\n" + description
				, vars = ["The type of the array elements."]
				}
			}
		where
			typec = toString (['{':k]++['}'])

			description = "These types of array are available:\n\n" +
				"- `{a}`, a normal array\n" +
				"- `{!a}`, a strict array (the elements are in root normal form)\n" +
				"- `{#a}`, an unboxed array (elements are stored directly, without pointers)\n" +
				"- `{32#a}`, a packed array (only `{32#Int}` and `{32#Real}`, for 32-bit integer and real arrays)"

	maybes = [make_maybe kind \\ kind <- [[], ['!'], ['#']]]
	where
		make_maybe :: [Char] -> TypeDefEntry
		make_maybe k =
			{ deft
			& tde_loc = Builtin (toString ['?':k`]) []
			, tde_typedef.td_name = toString ['_':k ++ ['Maybe']]
			, tde_typedef.td_args = [Var "a"]
			, tde_typedef.td_rhs = TDRCons False
				[ { defc & cons_name=toString ['?':k` ++ ['None']]}
				, { defc & cons_name=toString ['?':k` ++ ['Just']], cons_args=[Var "a"]}
				]
			, tde_doc = ?Just
				{ TypeDoc | gDefault{|*|}
				& description = ?Just $ "A maybe optionally contains a value." + description
				, vars = ["The type of the optional value."]
				}
			}
		where
			k` = case k of
				[]    -> ['^']
				['!'] -> []
				k     -> k
			defc =
				{ cons_name     = ""
				, cons_args     = []
				, cons_exi_vars = []
				, cons_context  = TypeContext []
				, cons_priority = ?None
				}

			description = "These types of maybes are available:\n\n" +
				"- {{`?a`}}, with a strict element\n" +
				"- {{`?^a`}}, with a lazy element (it may not be evaluated to head normal form)\n" +
				"- {{`?#a`}}, with an unboxed element (stored directly, without pointers)\n\n" +
				"The constructors {{`?|Just`}} and {{`?|None`}} are overloaded variants of the specific constructors."

	tuples = [make_tuple n \\ n <- [2..32]]
	where
		make_tuple :: Int -> TypeDefEntry
		make_tuple n =
			{ deft
			& tde_loc = Builtin typec [CLR "4.3"]
			, tde_typedef.td_name = "_Tuple" <+ n
			, tde_typedef.td_args = [Var $ toString [v:repeatn (n / 26) '`'] \\ v <- cycle ['a'..'z'] & n <- [0..n-1]]
			, tde_doc = ?Just
				{ TypeDoc | gDefault{|*|}
				& description = ?Just $ article + " " + ary + "ary tuple.\n\n" +
					"Tuples allow bundling a finite number of expressions of different types into one object without defining a new data type.\n\n" +
					"Clean supports tuples of arity 2 to 32."
				}
			}
		where
			typec = toString ['(':repeatn (n-1) ',' ++ [')']]
			ary = case n of
				2 -> "bin"
				3 -> "tern"
				n -> n <+ "-"
			article = if (n==8 || n==11 || n==18) "An" "A"
