definition module Util.License

/**
 * Utility functions to show license information in interactive programs.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class zero

from System.Options import :: Option

/**
 * Print the license details to stdout.
 * @param The name of the program.
 */
printLicenseDetails :: !String !*World -> *World

:: LicenseOptions =
	{ show_warranty   :: !Bool
	, show_conditions :: !Bool
	}

instance zero LicenseOptions

licenseOptions :: Option LicenseOptions

/**
 * Print warranty and/or redistribution conditions according to the
 * `LicenseOptions` on `stdout`.
 * @param The parsed options.
 * @result `True` if anything was shown.
 */
handleLicenseOptions :: !LicenseOptions !*World -> (!Bool, !*World)
