definition module Util.Cache

/**
 * Utility module for the Cloogle query cache.
 *
 * Copyright 2016-2022 the authors (see README.md).
 *
 * This file is part of cloogle-web.
 *
 * Cloogle-web is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, version 3 of the License.
 *
 * Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
 * for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.
 */

from StdOverloaded import class toString
from Text.GenJSON import generic JSONEncode, generic JSONDecode, :: JSONNode

:: CacheType = Brief | LongTerm
:: CacheKey :== String

cacheKey :: (a -> CacheKey) | toString a

/**
 * Check if for the hash of the argument a JSON file exists of type `b`.
 */
readCache :: !a !*World -> (! ?b, !*World) | toString a & JSONDecode{|*|} b

/**
 * All keys of a certain type currently in the cache. The list is sorted in
 * ascending order of acess time.
 */
allCacheKeys :: !CacheType !*World -> (![a], !*World) | JSONDecode{|*|} a

/**
 * Write for the hash of `a` a JSON file of type `b`.
 */
writeCache :: !CacheType !a !b !*World -> *World | toString, JSONEncode{|*|} a & JSONEncode{|*|} b

/**
 * Remove an entry from the cache.
 */
removeFromCache :: !CacheType !a -> *World -> *World | toString a
