# cloogle-web

This is a web frontend for [Cloogle][], a search engine for [Isocyanoclean][].
Cloogle lets you search for functions, types, classes and modules from
Isocyanoclean libraries. It also has documentation for language features,
common compiler errors and ABC instructions.

[Use Cloogle online!][cloogle.camilstaps.nl]

Cloogle was inspired by [Hoogle][]. As of June 27, 2018, Cloogle indexes
100&times; less lines of code than Hoogle (25 thousand vs. 2.3 million). This
allows Cloogle to implement more advanced features, such as `using` queries to
find usages.

---

**Readme contents**

[[_TOC_]]

---

## Auxiliary tools
Several tools used in Cloogle are available as separate libraries from
[gitlab.com/cloogle](https://gitlab.com/cloogle).
Additionally, these tools can be helpful:

- **[Cloogle-tags][cloogle-tags]:** lets you index local code in *tagfiles*
  that can be used by your text editor.
- **[Library browser](https://cloogle.camilstaps.nl/src):** the frontend
  includes a library browser to browse through all known libraries.
- **[Documentation browser](https://cloogle.camilstaps.nl/doc):** there is also
  an HTML version of the Clean Language Report.
- **[Statistics](https://cloogle.camilstaps.nl/stats):** long-term usage
  statistics.

## Interfacing with Cloogle
For a Clean interface to Cloogle, it is suggested that you use [libcloogle][].
For a Python interface to Cloogle, you can use [cloogle.py][].  A brief
description of the endpoints follows.

**TCP API**  
`CloogleServer` is a TCP server listening on port 31215 (typically). Send a
JSON request in the format described by the `Request` type in [Cloogle.API][],
followed by a newline character. The response is in the format described by the
`Response` type. The connection is kept alive for some time to allow further
requests.

To interface with Cloogle, it is recommended that you use the HTTP API rather
than the TCP API.

**HTTP API**  
The HTTP API is a simple wrapper around the TCP API, with a more stable API.
`api.php` should be called with a `GET` request where the `str` parameter
contains the search string. You may also add `mod` (a comma-separated list of
modules to search in) and `page` (for pagination: 0 for the first *n* results,
1 for the next *n*, etc.). For the query syntax in the `str` parameter, see the
'How to use' guidelines on the front page.

The API returns the same JSON as the TCP API, but may include additional
results when searched for Clean error messages (for example, `stack overflow`).
These error messages are indexed by the frontend rather than the backend.
Additionally, the HTTP API may give return codes above 150, which are not used
by the TCP API. For the meaning of the return codes, see [Cloogle.API (icl)][].

## Adding a new frontend / client to the statistics
If you have created a new Cloogle client, please give it a specific user agent
and add it to the `$user_agents` array in `frontend/stats/ajax/conf.php`. That
way, it will show up in the statistics on the `/stats` endpoint. There are no
strict naming conventions, but you can have a look at the other user agents for
inspiration.

Each entry has a key, which is the name of the client (e.g. 'vim-clean'). The
value is an array with a required `pattern`, which is used in SQL `LIKE`
queries so you can use `%` as in `%Linux%` - in most cases, the pattern will be
equal to the name. The optional `url` is a link to where the client may be used
or downloaded.

## Local setup
**Requirements**  
- 2GB of disk space for the source code and docker images.
- 1GB of RAM to build the index and 500MB while running.
- A high-speed CPU; instruction throughput is the primary bottleneck.

**Instructions**  
After installing [docker-compose][] run the following commands:

```bash
cp .env.example .env
touch cloogle.log
sudo docker-compose up
```

Your Cloogle server now runs at port `31215` on your local machine.
The web frontend is available at port `80`.

The server can be configured in the file `.env`. Some options are described
below.

**Statistics**  
Statistics are kept in a MySQL database. The easiest way to enable this is to
use `docker-compose --profile with-statistics`, which creates a dedicated
container with a MariaDB instance.

Alternatively, create a new database with `db/install.sql` elsewhere and set
the `CLOOGLE_DB_*` configuration values in `.env` to use this database instead.
A separate database is required if you run multiple instance and want them to
share statistics.

**Garbage collection**  
By default garbage collection for the query cache is disabled. To enable it,
use `docker-compose --profile with-garbage-collection`.

**Speeding up caching with `tmpfs`**  
To speed up caching it is wise to use a `tmpfs` for the `cache` directory. This
can be achieved by adding the following line to `/etc/fstab`:

```
tmpfs /path/to/cloogle-web/cache tmpfs defaults,atime,nosuid,nodev,mode=1666,size=100m 0 0
```

**Automatic updating**  
To update a Cloogle instance automatically, a cron job can be used, for
example:

```cron
0 3 * * * cd /path/to/cloogle-web; ./util/update.sh --no-interactive --no-clear-cache --mail-failure EMAIL_ADDRESS --release-directory /PATH/TO/RELEASES &>> /var/log/cloogle-build.log
```

See `./util/update.sh --help` for details about the options.

**Using nginx as a proxy**  
If you intend to run this on a server that has port 80 occupied already, you
can use nginx as a proxy. This is also needed if you want to set up multiple
instances on one server and use nginx to route requests to either instance
depending on the hostname or path.

Set `CLOOGLE_FRONTEND_PORT` to `127.0.0.1:31280` in `.env` and use the
following nginx config:

```nginx
server {
	listen [::]:80;
	server_name example.org;

	location / {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}
}
```

If you set up HTTPS on the proxy server, `/api.php` should still be accessible
on port 80 for `request` in Cloogle.Client to work. An example configuration
with HTTPS support:

```nginx
server {
	listen [::]:80;
	server_name example.org;

	location /api.php {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}

	location / {
		return 301 https://example.org$request_uri;
	}
}

server {
	listen [::]:443 ssl;
	server_name example.org;

	include /etc/nginx/confsnippets/ssl.conf; # Common SSL settings
	ssl_certificate /etc/letsencrypt/live/example.org/fullchain.pem;
	ssl_certificate_key /etc/letsencrypt/live/example.org/privkey.pem;

	location / {
		proxy_pass http://127.0.0.1:31280;
		proxy_set_header Host $host;
		proxy_set_header X-Forwarded-For $remote_addr;
	}
}
```

## Authors &amp; license
Cloogle is maintained by [Camil Staps][].

Copyright is owned by the authors of individual commits, including:

- Camil Staps
- Mart Lubbers
- Erin van der Veen
- Koen Dercksen
- Steffen Michels

This project is licensed under AGPL v3; see the [LICENSE](/LICENSE) file.

[Camil Staps]: https://camilstaps.nl
[cloogle.camilstaps.nl]: http://cloogle.camilstaps.nl
[Cloogle]: https://gitlab.com/cloogle/cloogle
[libcloogle]: https://gitlab.com/cloogle/libcloogle
[Cloogle.API]: https://gitlab.com/cloogle/libcloogle/blob/main/src/Cloogle/API.dcl
[Cloogle.API (icl)]: https://gitlab.com/cloogle/libcloogle/blob/main/src/Cloogle/API.icl
[cloogle-tags]: https://gitlab.com/cloogle/periphery/cloogle-tags
[cloogle.py]: https://gitlab.com/cloogle/periphery/cloogle.py

[Isocyanoclean]: https://clean-nc.camilstaps.nl
[vim-clean]: https://gitlab.com/clean-and-itasks/vim-clean

[docker-compose]: https://www.docker.com/products/docker-compose
[Hoogle]: https://github.com/ndmitchell/hoogle
