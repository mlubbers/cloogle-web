#!/usr/bin/env bash

# Copyright 2016-2022 the authors (see README.md).
#
# This file is part of cloogle-web.
#
# Cloogle-web is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, version 3 of the License.
#
# Cloogle-web is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License
# for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with cloogle-web. If not, see <https://www.gnu.org/licenses/>.

CACHE_SIZE=2000
INTERVAL=600
CACHE_DIR=/var/cache

cd "$CACHE_DIR"

while :; do
	# Long term cache
	cd lt
	n="$(ls -1 | wc -l)"
	if [ "$n" -gt "$CACHE_SIZE" ]; then
		ls -1tu | tail -n "$((n-CACHE_SIZE))" | xargs -P$(nproc) -r rm -v
	else
		echo "$n / $CACHE_SIZE cache entries used"
	fi
	cd ..
	# Brief cache
	cd brief
	n="$(ls -1 | wc -l)"
	echo "$n entries removed from brief cache"
	if [ "$n" -gt "0" ]; then rm *; fi
	cd ..
	# Wait
	sleep $INTERVAL
done
